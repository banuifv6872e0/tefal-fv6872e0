Tham khảo: https://hiyams.com/ban-ui-ban-la/ban-ui-hoi-nuoc/ban-ui-hoi-nuoc-tefal-fv6872e0/

Bàn ủi hơi nước Tefal FV6872E0 được trang bị Công nghệ nhiệt độ thông minh. Sự kết hợp hoàn hảo giữa nhiệt độ và hơi nước để bảo vệ các loại vải.
Bàn ủi hơi nước Tefal FV6872E0 nhờ các lỗ hơi được phân bổ đều, giúp khuếch tán hơi nước tối đa. Chức năng Chống nhỏ giọt sẽ giữ cho vải của bạn sạch sẽ, không có vết bẩn.
Bàn ủi hơi nước Tefal FV6872E0 với chức năng Tự động tắt máy đảm bảo an toàn khi sử dụng nếu bạn quên tắt máy. Chế độ Eco sẽ giúp bạn tiết kiệm điện hơn đến 20% so với mức tiêu thụ ở công suất tối đa.

Thông số kỹ thuật Bàn ủi hơi nước Tefal FV6872E0:

Công suất: 2600 W
Hơi phun: 40g/ phút
Hơi tăng cường: 260g/ phút
Mặt đế: Durilium, lướt nhanh, ủi phẳng, tránh bết rít
Hơi phun: Mạnh diệt khuẩn lên đến 99.99%
Lực phun: Mạnh, có thể ủi đứng
Tiết kiệm điện năng: Có
Dung tích bình chứa nước: 270ml
Chức năng ngắt điện tự động: Có
Hệ thống chống đóng cặn: Có
Xuất xứ: Trung Quốc
Kích thước: 34.0 x 14.0 x 18.0 cm (D x R x C)
Khối lượng: 1,434kg
Bảo hành: 2 năm